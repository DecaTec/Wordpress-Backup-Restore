#!/bin/bash

#
# Bash script for creating backups of Wordpress.
#
# Version 1.0.2
#
# Usage:
# 	- With backup directory specified in the script:  ./WordpressBackup.sh
# 	- With backup directory specified by parameter: ./WordpressBackup.sh <backupDirectory> (e.g. ./WordpressBackup.sh /media/hdd/wordpress_backup)
#

#
# IMPORTANT
# You have to customize this script (directories, users, etc.) for your actual environment.
# All entries which need to be customized are tagged with "TODO".
#

# Make sure the script exits when any command fails
set -Eeuo pipefail

# Variables
backupMainDir=${1:-} 

if [ -z "$backupMainDir" ]; then
    # TODO: The directory where you store the Wordpress backups (when not specified by args)
    backupMainDir='/media/hdd/wordpress_backup'
fi

# TODO: Use compression for Wordpress installation dir
# When this is the only script for backups, it's recommend to enable compression.
# If the output of this script is used in another (compressing) backup (e.g. borg backup), 
# you should probably disable compression here and only enable compression of your main backup script.
useCompression=true

# TOOD: The bare tar command for using compression.
# Use 'tar -cpzf' if you want to use gzip compression.
compressionCommand="tar -I pigz -cpf"

echo "Backup directory: $backupMainDir"

currentDate=$(date +"%Y%m%d_%H%M%S")

# The actual directory of the current backup - this is a subdirectory of the main directory above with a timestamp
backupDir="${backupMainDir}/${currentDate}"

# TODO: The directory of your Wordpress installation (this is a directory under your web root)
wordpressFileDir='/var/www/wordpress'

# TODO: The service name of the web server. Used to start/stop web server (e.g. 'systemctl start <webserverServiceName>')
webserverServiceName='nginx'

# TODO: Your Wordpress database name
wordpressDatabase='wordpress_db'

# TODO: Your Wordpress database user
dbUser='wordpress_db_user'

# TODO: The password of the Wordpress database user
dbPassword='mYpAsSw0rd'

# TODO: The maximum number of backups to keep (when set to 0, all backups are kept)
maxNrOfBackups=0

# File name for file backup
# If you prefer another file name, you'll also have to change the WordpressRestore.sh script.
fileNameBackupFileDir='wordpress-filedir.tar'

if [ "$useCompression" = true ] ; then
	fileNameBackupFileDir='wordpress-filedir.tar.gz'
fi

# File name for database dump
fileNameBackupDb='wordpress-db.sql'

# Function for error messages
errorecho() { cat <<< "$@" 1>&2; }

# Capture CTRL+C
trap CtrlC INT

function CtrlC() {
	echo "Backup cancelled."
	exit 1
}

#
# Check for root
#
if [ "$(id -u)" != "0" ]
then
	errorecho "ERROR: This script has to be run as root!"
	exit 1
fi

#
# Check if backup dir already exists
#
if [ ! -d "${backupDir}" ]
then
	mkdir -p "${backupDir}"
else
	errorecho "ERROR: The backup directory ${backupDir} already exists!"
	exit 1
fi

#
# Stop web server
#
echo "$(date +"%H:%M:%S"): Stopping web server..."
systemctl stop "${webserverServiceName}"
echo "Done"
echo

#
# Backup file directory
#
echo "$(date +"%H:%M:%S"): Creating backup of Wordpress file directory..."
if [ "$useCompression" = true ] ; then
	`$compressionCommand "${backupDir}/${fileNameBackupFileDir}" -C "${wordpressFileDir}" .`
else
	tar -cpf "${backupDir}/${fileNameBackupFileDir}" -C "${wordpressFileDir}" .
fi
echo "Done"
echo

#
# Backup DB
#
echo "$(date +"%H:%M:%S"): Backup Wordpress database..."

if ! [ -x "$(command -v mysqldump)" ]; then
    errorecho "ERROR: MySQL/MariaDB not installed (command mysqldump not found)."
    errorecho "ERROR: No backup of database possible!"
else
    mysqldump --single-transaction -h localhost -u "${dbUser}" -p"${dbPassword}" "${wordpressDatabase}" > "${backupDir}/${fileNameBackupDb}"
fi

echo "Done"
echo


#
# Start web server
#
echo "$(date +"%H:%M:%S"): Starting web server..."
systemctl start "${webserverServiceName}"
echo "Done"
echo


#
# Delete old backups
#
if [ ${maxNrOfBackups} != 0 ]
then
	nrOfBackups=$(ls -l ${backupMainDir} | grep -c ^d)

	if [[ ${nrOfBackups} > ${maxNrOfBackups} ]]
	then
		echo "$(date +"%H:%M:%S"): Removing old backups..."
		ls -t ${backupMainDir} | tail -$(( nrOfBackups - maxNrOfBackups )) | while read -r dirToRemove; do
			echo "${dirToRemove}"
			rm -r "${backupMainDir}/${dirToRemove:?}"
			echo "Done"
			echo
		done
	fi
fi

echo
echo "DONE!"
echo "$(date +"%H:%M:%S"): Backup created: ${backupDir}"